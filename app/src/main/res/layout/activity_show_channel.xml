<?xml version="1.0" encoding="utf-8"?><!--
    Copyright 2020 Thomas Schneider

    This file is a part of TubeLab

    This program is free software; you can redistribute it and/or modify it under the terms of the
    GNU General Public License as published by the Free Software Foundation; either version 3 of the
    License, or (at your option) any later version.

    TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
    the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
    Public License for more details.

    You should have received a copy of the GNU General Public License along with TubeLab; if not,
    see <http://www.gnu.org/licenses>.
-->
<androidx.coordinatorlayout.widget.CoordinatorLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:fitsSystemWindows="true"
    tools:context=".ShowChannelActivity">

    <com.google.android.material.appbar.AppBarLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:fitsSystemWindows="true"
        app:theme="@style/ThemeOverlay.AppCompat.ActionBar">

        <com.google.android.material.appbar.CollapsingToolbarLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:fitsSystemWindows="true"
            app:contentScrim="?attr/colorPrimary"
            app:expandedTitleMarginEnd="64dp"
            app:expandedTitleMarginStart="48dp"
            app:layout_scrollFlags="scroll|exitUntilCollapsed">

            <RelativeLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content">

                <androidx.constraintlayout.widget.ConstraintLayout
                    android:id="@+id/top_banner"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="5dp">

                    <ImageView
                        android:id="@+id/account_pp"
                        android:layout_width="80dp"
                        android:layout_height="80dp"
                        android:layout_margin="10dp"
                        android:layout_marginTop="10dp"
                        android:background="@drawable/account_pp_border"
                        android:contentDescription="@string/profile_picture"
                        android:padding="2dp"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toTopOf="parent" />

                    <TextView
                        android:id="@+id/account_dn"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="10dp"
                        android:singleLine="true"
                        android:textAppearance="@style/TextAppearance.AppCompat.Body1"
                        android:textColor="@android:color/white"
                        android:textSize="18sp"
                        app:layout_constraintBottom_toTopOf="@+id/subscriber_count"
                        app:layout_constraintStart_toEndOf="@+id/account_pp"
                        app:layout_constraintTop_toTopOf="@+id/account_pp" />

                    <TextView
                        android:id="@+id/subscriber_count"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="10dp"
                        android:layout_marginTop="10dp"
                        android:gravity="center_vertical"
                        android:singleLine="true"
                        android:textAppearance="@style/TextAppearance.AppCompat.Body1"
                        android:textColor="@android:color/white"
                        android:visibility="invisible"
                        app:layout_constraintBottom_toBottomOf="@id/account_follow"
                        app:layout_constraintStart_toEndOf="@+id/account_pp"
                        app:layout_constraintTop_toBottomOf="@id/account_dn" />

                    <Button
                        android:id="@+id/account_follow"
                        style="@style/Widget.AppCompat.Button.Colored"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="10dp"
                        android:contentDescription="@string/make_an_action"
                        android:scaleType="fitCenter"
                        android:visibility="gone"
                        app:layout_constraintStart_toEndOf="@id/subscriber_count"
                        app:layout_constraintTop_toBottomOf="@id/account_dn" />

                    <Button
                        style="@style/Widget.AppCompat.Button.Colored"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginStart="10dp"
                        android:contentDescription="@string/edit_profile"
                        android:scaleType="fitCenter"
                        android:src="@drawable/ic_baseline_edit_24"
                        android:visibility="gone"
                        app:layout_constraintStart_toEndOf="@id/subscriber_count"
                        app:layout_constraintTop_toBottomOf="@id/account_dn" />


                </androidx.constraintlayout.widget.ConstraintLayout>

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_below="@+id/top_banner"
                    android:gravity="center"
                    android:orientation="vertical"
                    android:paddingTop="8dp"
                    app:layout_scrollFlags="scroll|enterAlways">

                    <TextView
                        android:id="@+id/account_note"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="5dp"
                        android:gravity="center"
                        android:padding="10dp"
                        android:textColor="@android:color/white"
                        android:textIsSelectable="true"
                        android:visibility="gone" />
                </LinearLayout>
            </RelativeLayout>


        </com.google.android.material.appbar.CollapsingToolbarLayout>


        <com.google.android.material.tabs.TabLayout
            android:id="@+id/account_tabLayout"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:tabGravity="fill"
            app:tabMode="fixed"
            app:tabSelectedTextColor="?colorAccent"
            app:tabTextColor="@android:color/white" />
    </com.google.android.material.appbar.AppBarLayout>

    <androidx.viewpager.widget.ViewPager
        android:id="@+id/account_viewpager"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="5dp"
        app:layout_behavior="@string/appbar_scrolling_view_behavior" />

    <com.google.android.material.floatingactionbutton.FloatingActionButton
        android:id="@+id/action_button"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="bottom|end"
        android:layout_margin="@dimen/fab_margin_button"
        android:src="@drawable/ic_baseline_add_24"
        android:tint="@android:color/white"
        android:visibility="gone" />

</androidx.coordinatorlayout.widget.CoordinatorLayout>