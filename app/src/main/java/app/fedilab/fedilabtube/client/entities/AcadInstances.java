package app.fedilab.fedilabtube.client.entities;
/* Copyright 2021 Thomas Schneider
 *
 * This file is a part of TubeLab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * TubeLab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with TubeLab; if not,
 * see <http://www.gnu.org/licenses>. */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import app.fedilab.fedilabtube.helper.HelperAcadInstance;

public class AcadInstances {

    private String name;
    private String url;
    private boolean openId;

    public static boolean isOpenId(String domain) {
        List<AcadInstances> instances = getInstances();
        for (AcadInstances acadInstance : instances) {
            if (acadInstance.getUrl().compareTo(domain) == 0) {
                return acadInstance.isOpenId();
            }
        }
        return false;
    }

    public static List<AcadInstances> getInstances() {
        List<AcadInstances> acadInstances = new ArrayList<>();

        for (String academie : HelperAcadInstance.academies) {
            AcadInstances acadInstance = new AcadInstances();
            acadInstance.name = academie;
            acadInstance.openId = !Arrays.asList(HelperAcadInstance.notOpenId).contains(academie);
            acadInstance.url = getPeertubeUrl(academie);
            acadInstances.add(acadInstance);
        }
        HashMap<String, String> instancesMap = new HashMap<>(HelperAcadInstance.instances_themes);
        Iterator<Map.Entry<String, String>> it = instancesMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            AcadInstances acadInstance = new AcadInstances();
            acadInstance.name = pair.getKey();
            acadInstance.openId = true;
            acadInstance.url = pair.getValue();
            acadInstances.add(acadInstance);
            it.remove();
        }
        return acadInstances;
    }

    /**
     * Returns the peertube URL depending of the academic domain name
     *
     * @param acad String academic domain name
     * @return String the peertube URL
     */
    private static String getPeertubeUrl(String acad) {

        if (acad.compareTo("education.gouv.fr") == 0 || acad.compareTo("igesr.gouv.fr") == 0) {
            acad = "education.fr";
        } else if (acad.compareTo("ac-nancy-metz.fr") == 0) {
            acad = "ac-nancy.fr";
        } else if (acad.compareTo("clermont.fr") == 0) {
            acad = "clermont-ferrand.fr";
        } else if (acad.compareTo("ac-wf.wf") == 0 || acad.compareTo("ac-mayotte.fr") == 0 || acad.compareTo("ac-noumea.nc") == 0
                || acad.compareTo("ac-guadeloupe.fr") == 0 || acad.compareTo("monvr.pf") == 0 || acad.compareTo("ac-reunion.fr") == 0 ||
                acad.compareTo("ac-martinique.fr") == 0 || acad.compareTo("ac-guyane.fr") == 0
        ) {
            acad = "outremer.fr";
        }
        if (!acad.contains("ac-lyon.fr")) {
            //TODO: remove hack for test with openid
            /*if( acad.contains("orleans-tours.fr")) {
                return "tube-normandie.beta.education.fr";
            }*/
            return "tube-" + acad.replaceAll("ac-|\\.fr", "") + ".beta.education.fr";
        } else {
            return "tube.ac-lyon.fr";
        }
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public boolean isOpenId() {
        return openId;
    }

}
